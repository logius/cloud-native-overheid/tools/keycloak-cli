package config

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

// GroupConfig spec
type GroupConfig struct {
	Name    string   `yaml:"name"`
	Admin   bool     `yaml:"admin"`
	Members []string `yaml:"members"`
}

// Customer configuration
type Customer struct {
	Name   string
	Groups []GroupConfig
}

// Config contains customer configuration
type Config struct {
	Customer Customer
}

// LoadTenantConfig reads config from YAML
func LoadTenantConfig(fileName string) *Config {

	var config Config
	readConfig(fileName, &config)

	return &config
}

func readConfig(fileName string, config interface{}) {
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatalf("Error reading YAML file: %s\n", err)
	}

	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		log.Fatalf("Error parsing YAML file: %s\n", err)
	}
}
