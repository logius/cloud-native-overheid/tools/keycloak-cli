package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/group"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/testusers"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "keycloak",
		Short: "OPS tools for KeyCloak",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(group.NewCommand())
	cmd.AddCommand(testusers.NewCommand())
}
