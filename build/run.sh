#!/bin/bash

IMAGE=registry.gitlab.com/logius/cloud-native-overheid/components/keycloak-cli:local

docker run --rm -it \
    -v $KUBECONFIG:/.kube/config \
    -v $PWD/examples:/examples \
    $IMAGE 

