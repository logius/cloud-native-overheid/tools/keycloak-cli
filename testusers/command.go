package testusers

import (
	"fmt"
	"log"

	"github.com/Nerzal/gocloak/v8"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	config      *string
	keycloakURL *string
	oidcRealm   *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return createTestUsers(cmd, &flags)
		},
		Use:   "create_test_users",
		Short: "Create test users",
		Long:  "This command creates a set if test users in KeyCloak.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.keycloakURL = cmd.Flags().String("url", "", "URL of KeyCloak")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("url")

	return cmd
}

func createTestUsers(cmd *cobra.Command, flags *flags) error {

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	for i := 1; i <= 10; i++ {
		username := fmt.Sprintf("tester%d", i)
		email := fmt.Sprintf("tester%d@x.nl", i)
		firstName := "Test"
		lastName := fmt.Sprintf("User%d", i)

		user := &gocloak.User{
			Username:  &username,
			Email:     &email,
			FirstName: &firstName,
			LastName:  &lastName,
		}
		log.Printf("Create user %q", username)
		err := keyCloakClient.CreateUser(user)
		if err != nil {
			return err
		}
	}
	return nil
}
