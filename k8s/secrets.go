package k8s

import (
	"context"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetKeycloakSecrets() (username string, password string) {

	secretName := "keycloak-http"

	k8sClient = initK8sClient()

	opts := metav1.ListOptions{
		LabelSelector: "app.kubernetes.io/instance = keycloak",
	}
	statefulSets, err := k8sClient.AppsV1().StatefulSets("").List(context.TODO(), opts)
	if err != nil || len(statefulSets.Items) == 0 {
		log.Fatalf("Could not find keycloak statefulset by label %q", opts.LabelSelector)
	}
	secret, err := k8sClient.CoreV1().Secrets(statefulSets.Items[0].Namespace).Get(context.TODO(), "keycloak-http", metav1.GetOptions{})
	if err != nil {
		log.Fatalf("%v", err)
	}

	username = string(secret.Data["username"])
	if username == "" {
		log.Fatalf("Missing username in secret %q", secretName)
	}
	password = string(secret.Data["password"])
	if password == "" {
		log.Fatalf("Missing password in secret %q", secretName)
	}
	return username, password
}
