package keycloak

import (
	"context"
	"fmt"

	"github.com/Nerzal/gocloak/v8"
)

// GetGroups fetches all groups from KeyCloak
func (keyCloakClient Client) GetGroups() ([]*gocloak.Group, error) {
	briefRepresentation := false
	groups, err := keyCloakClient.gocloak.GetGroups(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetGroupsParams{
		BriefRepresentation: &briefRepresentation,
	})
	if err != nil {
		return nil, err
	}
	return groups, nil
}

// CreateGroup creates a group in KeyCloak
func (keyCloakClient Client) CreateGroup(groupName string, tenant string) error {
	path := "/" + groupName
	attributes := map[string][]string{
		"Tenant": {tenant},
	}
	group := gocloak.Group{
		Name:       &groupName,
		Path:       &path,
		Attributes: &attributes,
	}
	_, err := keyCloakClient.gocloak.CreateGroup(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, group)
	return err
}

// RemoveGroup removes a group from KeyCloak
func (keyCloakClient Client) RemoveGroup(groupID string) error {
	err := keyCloakClient.gocloak.DeleteGroup(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, groupID)
	return err
}

// GetGroupID gets a group from KeyCloak and returns the ID
func (keyCloakClient Client) GetGroupID(groupName string) (string, error) {

	groups, err := keyCloakClient.gocloak.GetGroups(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetGroupsParams{
		Search: &groupName,
	})
	if err != nil {
		return "", err
	}
	for _, group := range groups {
		if *group.Name == groupName {
			return *group.ID, nil
		}
	}
	return "", fmt.Errorf("group %q was not found in KeyCloak", groupName)
}

// GetGroupMembers gets members of a Group
func (keyCloakClient Client) GetGroupMembers(groupID string) ([]*gocloak.User, error) {
	members, err := keyCloakClient.gocloak.GetGroupMembers(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, groupID, gocloak.GetGroupsParams{})
	if err != nil {
		return nil, err
	}
	return members, nil
}

// RemoveMemberFromGroup removes a member from a Group
func (keyCloakClient Client) RemoveMemberFromGroup(userid string, groupID string) error {
	err := keyCloakClient.gocloak.DeleteUserFromGroup(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, userid, groupID)
	return err
}

// AddMemberToGroup adds a member to a Group
func (keyCloakClient Client) AddMemberToGroup(userid string, groupID string) error {
	err := keyCloakClient.gocloak.AddUserToGroup(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, userid, groupID)
	return err
}
