package keycloak

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/Nerzal/gocloak/v8"
)

// Client struct with api methods and accesstoken
type Client struct {
	gocloak gocloak.GoCloak
	token   *gocloak.JWT
	realm   string
}

// NewKeyCloakClient initializes a new Client struct and connects to Keycloak
func NewKeyCloakClient(keycloakURL string, realm string) (*Client, error) {

	var keyCloakClient = &Client{
		realm: realm,
	}

	log.Printf("Init Keycloak Client")

	keycloakUser := os.Getenv("KEYCLOAK_USERNAME")
	keycloakPassword := os.Getenv("KEYCLOAK_PASSWORD")

	if keycloakUser == "" {
		return nil, fmt.Errorf("missing environment variable KEYCLOAK_USERNAME")
	}
	if keycloakPassword == "" {
		return nil, fmt.Errorf("missing environment variable KEYCLOAK_PASSWORD")
	}

	gocloak := gocloak.NewClient(keycloakURL)
	token, err := gocloak.LoginAdmin(context.Background(), keycloakUser, keycloakPassword, "master")
	if err != nil {
		return nil, fmt.Errorf("something wrong with the credentials or url %w", err)
	}

	keyCloakClient.gocloak = gocloak
	keyCloakClient.token = token

	return keyCloakClient, nil
}
