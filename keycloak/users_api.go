package keycloak

import (
	"context"
	"fmt"

	"github.com/Nerzal/gocloak/v8"
)

// GetUserID gets the ID of a user
func (keyCloakClient Client) GetUserID(username string) (string, error) {
	user, err := keyCloakClient.GetUser(username)
	if err != nil {
		return "", err
	}
	return *user.ID, nil
}

// GetUser gets a User from KeyCloak
func (keyCloakClient Client) GetUser(username string) (*gocloak.User, error) {
	users, err := keyCloakClient.gocloak.GetUsers(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetUsersParams{
		Username: &username,
	})
	if err != nil {
		return nil, err
	}
	for _, user := range users {
		if *user.Username == username {
			return user, nil
		}
	}

	return nil, fmt.Errorf("user %q was not found in KeyCloak", username)
}

// GetAllUsers gets all Users from Keycloak
func (keyCloakClient Client) GetAllUsers() ([]*gocloak.User, error) {
	count, err := keyCloakClient.GetUserCount()
	if err != nil {
		return nil, err
	}
	users, err := keyCloakClient.gocloak.GetUsers(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetUsersParams{Max: &count})
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (keyCloakClient Client) GetUserCount() (int, error) {
	userCount, err := keyCloakClient.gocloak.GetUserCount(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetUsersParams{})
	if err != nil {
		return 0, err
	}
	return userCount, nil
}

// Check if a user exists in keycloak and return true if it does, false if it does not
func (keyCloakClient Client) UserExists(username string) (bool, error) {
	users, err := keyCloakClient.gocloak.GetUsers(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetUsersParams{
		Username: &username,
	})
	if err != nil {
		return false, err
	}

	//Only accept exact matches for username
	for _, user := range users {
		if *user.Username == username {
			return true, nil
		}
	}
	return false, nil
}

// SearchUser searches a User in KeyCloak
func (keyCloakClient Client) SearchUser(username string) (*gocloak.User, error) {
	users, err := keyCloakClient.gocloak.GetUsers(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, gocloak.GetUsersParams{
		Username: &username,
	})
	if err != nil {
		return nil, err
	}

	for _, user := range users {
		if *user.Username == username {
			return user, nil
		}
	}

	return nil, nil
}

// CreateUser creates a user in KeyCloak
func (keyCloakClient Client) CreateUser(user *gocloak.User) error {
	_, err := keyCloakClient.gocloak.CreateUser(context.TODO(), keyCloakClient.token.AccessToken, keyCloakClient.realm, *user)
	return err
}
