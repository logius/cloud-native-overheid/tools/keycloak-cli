module gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli

go 1.15

require (
	github.com/Nerzal/gocloak/v8 v8.1.1
	github.com/spf13/cobra v1.1.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
)
