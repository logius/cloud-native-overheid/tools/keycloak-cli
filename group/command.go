package group

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

type flags struct {
	config      *string
	keycloakURL *string
	oidcRealm   *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureGroups(cmd, &flags)
		},
		Use:   "configure-groups",
		Short: "Configure KeyCloak groups",
		Long:  "This command configures groups in KeyCloak.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.keycloakURL = cmd.Flags().String("url", "", "URL of KeyCloak")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("url")

	return cmd
}

func configureGroups(cmd *cobra.Command, flags *flags) error {

	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}
	groupReconciler := Reconciler{
		TenantConfig:   &config.LoadTenantConfig(*flags.config).Customer,
		KeyCloakClient: keyCloakClient,
	}

	err = groupReconciler.ReconcileGroups()
	return err
}
