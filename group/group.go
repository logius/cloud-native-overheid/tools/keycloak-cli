package group

import (
	"log"
	"sync"

	"github.com/Nerzal/gocloak/v8"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
)

// Reconciler configures groups and members in KeyCloak according to a YAML config
type Reconciler struct {
	TenantConfig   *config.Customer
	KeyCloakClient *keycloak.Client
}

// ReconcileGroups read TenantConfig and create and deletes KeyCloak groups accordingly.
func (groupReconciler Reconciler) ReconcileGroups() error {
	var wg sync.WaitGroup

	// find all KeyCloak groups
	keyCloakGroups, err := groupReconciler.KeyCloakClient.GetGroups()
	if err != nil {
		return err
	}
	// filter groups for current tenant
	tenantKeyCloakGroups := filterTenantGroups(groupReconciler.TenantConfig.Name, keyCloakGroups)

	// handle current groups in tenant config
	for _, groupConfig := range groupReconciler.TenantConfig.Groups {
		wg.Add(1)
		go func(groupConfig config.GroupConfig) {
			defer wg.Done()
			err := groupReconciler.reconcileGroup(groupConfig, tenantKeyCloakGroups)
			if err != nil {
				log.Fatal(err) //Direct Fatal, cannot bubble errors from goroutines
			}
		}(groupConfig)
	}

	// handle groups removed from tenant config
	for _, keyCloakGroup := range tenantKeyCloakGroups {
		if !groupReconciler.isKeyCloakGroupRequired(keyCloakGroup) {
			wg.Add(1)
			go func(keyCloakGroup *gocloak.Group) {
				defer wg.Done()
				err := groupReconciler.removeGroup(keyCloakGroup)
				if err != nil {
					log.Fatal(err) //Direct Fatal, cannot bubble errors from goroutines
				}
			}(keyCloakGroup)
		}
	}
	wg.Wait()
	return nil
}

func (groupReconciler Reconciler) removeGroup(keyCloakGroup *gocloak.Group) error {
	log.Printf("Remove KeyCloak group %q\n", *keyCloakGroup.Name)
	return groupReconciler.KeyCloakClient.RemoveGroup(*keyCloakGroup.ID)
}

func (groupReconciler Reconciler) isKeyCloakGroupRequired(keyCloakGroup *gocloak.Group) bool {
	for _, groupConfig := range groupReconciler.TenantConfig.Groups {
		if groupConfig.Name == *keyCloakGroup.Name {
			return true
		}
	}
	return false
}

func (groupReconciler Reconciler) reconcileGroup(groupConfig config.GroupConfig, tenantKeyCloakGroups []*gocloak.Group) error {
	log.Printf("Reconcile KeyCloak group %q\n", groupConfig.Name)

	keyCloakGroupID, exists := getGroupID(tenantKeyCloakGroups, groupConfig.Name)

	if !exists {
		log.Printf("Create KeyCloak group %s\n", groupConfig.Name)
		err := groupReconciler.KeyCloakClient.CreateGroup(groupConfig.Name, groupReconciler.TenantConfig.Name)
		if err != nil {
			return err
		}

		keyCloakGroupID, err = groupReconciler.KeyCloakClient.GetGroupID(groupConfig.Name)
		if err != nil {
			return err
		}
	}
	return groupReconciler.groupMembers(groupConfig, keyCloakGroupID)
}

func (groupReconciler Reconciler) groupMembers(groupConfig config.GroupConfig, keyCloakGroupID string) error {
	members, err := groupReconciler.KeyCloakClient.GetGroupMembers(keyCloakGroupID)
	if err != nil {
		return err
	}
	memberUserNames := getUserNames(members)
	log.Printf("Current members in Keycloak group %q: %q\n", groupConfig.Name, memberUserNames)

	membersToBeRemoved := getMembersToBeRemoved(groupConfig, members)
	for _, memberToBeRemoved := range membersToBeRemoved {
		log.Printf("Remove user %q from KeyCloak group %q\n", *memberToBeRemoved.Username, groupConfig.Name)
		err := groupReconciler.KeyCloakClient.RemoveMemberFromGroup(*memberToBeRemoved.ID, keyCloakGroupID)
		if err != nil {
			return err
		}
	}

	membersToBeAdded := getMembersToBeAdded(groupConfig, memberUserNames)
	for _, memberToBeAdded := range membersToBeAdded {
		err := groupReconciler.addMemberToGroup(groupConfig, keyCloakGroupID, memberToBeAdded)
		if err != nil {
			return err
		}
	}
	return nil
}

func (groupReconciler Reconciler) addMemberToGroup(groupConfig config.GroupConfig, keyCloakGroupID string, member string) error {
	log.Printf("Add user %q to KeyCloak group %q\n", member, groupConfig.Name)
	keyCloakUserID, err := groupReconciler.KeyCloakClient.GetUserID(member)
	if err != nil {
		return err
	}
	if keyCloakUserID != "" {
		err = groupReconciler.KeyCloakClient.AddMemberToGroup(keyCloakUserID, keyCloakGroupID)
		if err != nil {
			return err
		}
	}
	return nil
}

func filterTenantGroups(tenant string, keyCloakGroups []*gocloak.Group) []*gocloak.Group {
	tenantKeyCloakGroups := make([]*gocloak.Group, 0)
	for _, keyCloakGroup := range keyCloakGroups {
		if len((*keyCloakGroup.Attributes)["Tenant"]) > 0 && (*keyCloakGroup.Attributes)["Tenant"][0] == tenant {
			tenantKeyCloakGroups = append(tenantKeyCloakGroups, keyCloakGroup)
		}
	}
	return tenantKeyCloakGroups
}

func getUserNames(currentMembers []*gocloak.User) []string {
	usernames := make([]string, len(currentMembers))
	for i, member := range currentMembers {
		usernames[i] = *member.Username
	}
	return usernames
}

func getMembersToBeRemoved(groupConfig config.GroupConfig, currentMembers []*gocloak.User) []*gocloak.User {
	var membersToBeRemoved []*gocloak.User
	for _, member := range currentMembers {
		if !stringInSlice(*member.Username, groupConfig.Members) {
			membersToBeRemoved = append(membersToBeRemoved, member)
		}
	}
	return membersToBeRemoved
}

func getMembersToBeAdded(groupConfig config.GroupConfig, currentMembers []string) []string {
	var membersToBeAdded []string
	for _, member := range groupConfig.Members {
		if !stringInSlice(member, currentMembers) {
			membersToBeAdded = append(membersToBeAdded, member)
		}
	}
	return membersToBeAdded
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func getGroupID(groups []*gocloak.Group, groupName string) (keyCloakGroupID string, exists bool) {
	for _, group := range groups {
		if *group.Name == groupName {
			keyCloakGroupID = *group.ID
			exists = true
			return
		}
	}
	return
}
