package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/commands"
)

func main() {
	rootCmd := &cobra.Command{}
	commands.AddSubCommands(rootCmd)
	rootCmd.SilenceUsage = true

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
